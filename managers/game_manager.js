//@ts-check
const ClientRepo = require('../client_repository/ClientRepository');

/**
 * 
 * @param {*} app 
 * @param {ClientRepo} clientRepository 
 */

module.exports = function(app, clientRepository){
	
	app.onReceive('startgame', (params) => {
        let p = String(params);
        const playerId = parseInt( p.split(' ')[1] );
        const state = parseInt( p.split(' ')[0] );

        if(state == 1){
            clientRepository.broadcastAll('newgame 0 1 2 1 1 1 18 33333333333333555555555555552222222222222224444444444444466666666666666777777777777771111111111111111111111111111111111111111111111112222222222222222222234444444444444566666666666666678888889999999999 0 1');
        } else {
            clientRepository.broadcastAll('endgame');
        }
    });

    app.onReceive('f', (params) => {
        let p = String(params);
        const playerNum = parseInt( p.split(' ')[0] );
        const field = p.split(' ').slice(1).join(' ');
        const message = `f ${playerNum} ${field}`;
        clientRepository.broadcastAll(message); 
    });
    
    app.onReceive('lvl', (params) => {
        params = String(params);
	
    });
    
    app.onReceive('sb', (params) => {
        const splitParams = String(params).split(' ');
        const sendernum = parseInt(splitParams[2]);
        clientRepository.broadcastAllExcept(`sb ${params}`, sendernum);
    });

    app.onReceive('playerlost', (params) => {
        clientRepository.broadcastAll(`playerlost ${params}`); 
        console.log(params);
    });

    app.onReceive('pause', (params) => {
        let p = String(params);
        const state = parseInt( p.split(' ')[0] );
        clientRepository.broadcastAll(`pause ${state}`);
        // pause e resume game
    });

}